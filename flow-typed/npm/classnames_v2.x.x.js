// flow-typed signature: b60efd6d203942ef2297c1d32c9611bc
// flow-typed version: 2b8923a76c/classnames_v2.x.x/flow_>=v0.25.x

type $npm$classnames$Classes =
  | string
  | { [className: string]: * }
  | false
  | void
  | null;

declare module "classnames" {
  declare function exports(
    ...classes: Array<$npm$classnames$Classes | Array<$npm$classnames$Classes>>
  ): string;
}

declare module "classnames/bind" {
  declare module.exports: $Exports<"classnames">;
}

declare module "classnames/dedupe" {
  declare module.exports: $Exports<"classnames">;
}
