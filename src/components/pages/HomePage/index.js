// https://github.com/diegohaz/arc/wiki/Atomic-Design
import React from 'react';
import Helmet from 'react-helmet';
import PageTemplate from '../../templates/PageTemplate';
import ContentArea from '../../atoms/ContentArea/ContentArea';

const HomePage = () => (
  <PageTemplate title="Lotto Demo">
    <Helmet>
      <title>Home</title>
    </Helmet>
    <ContentArea padded fullHeight>
      <p>This is a simple test for Lottostarlet.</p>
      <p>There's going to be a few difference here. Mainly with the Menu not using the icons from the design.</p>
      <p>The only way to make that work is to same these nav items as image buttons. But that would be wrong.</p>
      <p>The icons would be taken from the design in vector format to be converted to an SVG or font icon. This way, everything is rendered correctly, and SVG's will be embedded into the document. This means NO extra requests for the browser/mobile devices and super fast load times. </p>
      <p>This is also a SPA (Facebook's React) but with the ability to render pages on the server including data. So you can still keep all that great SEO you'd loose with a traditional SPA.</p>
      <p>Anyway, I've been doing this for over 20 years now, and know quite a bit more than just this. So give us a shout to find out more.</p>
    </ContentArea>
  </PageTemplate>
);

export default HomePage;
