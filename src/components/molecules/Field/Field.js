import React, { PureComponent } from 'react';
import styled from 'styled-components';
import InputText from '../../atoms/InputText/InputText';
import { LabelProps } from '../../atoms/Label/LabelProps';
import Label from '../../atoms/Label/Label';

const InputColumn = styled.div`
  display: block;
  margin-bottom: .5rem;
  position: relative;
`;

const InputLabel = styled(Label)`
  position: absolute;
  left: .7rem;
  font-size: 0.7rem;
  ${({ hasValue }) => (hasValue ? 'visiblity: visible;' : 'visibility: hidden;')};
  ${({ errors }) => (errors ? 'color: red;' : 'color: inherit;')};
`;

const Required = styled.span`
  color: tomato;
`;

const Message = styled.div`
  margin-top: .1rem;
`;

const Errors = styled.div`
  color: tomato;
`;

class Field extends PureComponent {
  static propTypes = {
    ...InputText.PropTypes,
    ...LabelProps,
  };
  constructor(props) {
    super(props);
    this.state = { hasValue: false };
  }
  /* componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.forceUpdate();
    }
  } */
  onChange = event => {
    this.value = event.target.value;
    if (this.props.onChange) { this.props.onChange(event); } // Pass the onChange to the parent
    this.setState({ hasValue: true });
    // console.log('State', this.state);
  };
  render() {
    const { name, label, type, placeholder, message, value, required, onChange, dirty, touched, errors, ...rest } = this.props;
    return (
      <InputColumn>
        { label && <InputLabel hasValue={this.state.hasValue} errors={errors}>{ label } { required && <Required>*</Required>}</InputLabel> }
        <InputText type={type} placeholder={placeholder} name={name} value={value || this.value || ''} errors={errors} onChange={this.onChange} {...rest} />
        {/* {touched && errors && <Errors>{errors}</Errors>} */}
        {message && <Message>{message}</Message>}
      </InputColumn>
    );
  }
}
export default Field;
