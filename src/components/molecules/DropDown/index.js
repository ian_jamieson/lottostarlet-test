import React, { PureComponent } from 'react';
import styled, { keyframes } from 'styled-components';
import { bounceInDown, bounceOutUp } from 'react-animations';
import PropTypes from 'prop-types';
import TetherComponent from 'react-tether';

const bounceInAnimation = keyframes`${bounceInDown}`;
const bounceOutAnimation = keyframes`${bounceOutUp}`;

const DropdownWrapper = styled.div`
  position: relative;
  cursor: pointer;
  ${({ isOpen }) => (isOpen ? 'background: linear-gradient(white 0%, white 95%, rgb(218,216,216) 95%, rgb(218,216,216) 100%)' : 'background: transparent')};
`;

const DropDownContent = styled.div`
  background: rgb(244,244,244);
  border: solid 1px rgb(218,216,216);
  border-bottom-left-radius: .3rem;
  border-bottom-right-radius: .3rem;
  min-width: 240px;
  @media(min-width: 1024px) {
    width: 350px;
  }
  animation: 1s ${({ isOpen }) => (isOpen ? `${bounceInAnimation}` : `${bounceOutAnimation}`)};
`;

const DropdownHeader = styled.header`
  padding: 1rem;
  text-align: center;
  font-weight: 600;
  font-size: 1.1rem;
  background: rgb(218,216,216);
`;


class DropDown extends PureComponent {
  static propTypes = {
    children: PropTypes.any.isRequired,
    content: PropTypes.any.isRequired,
    title: PropTypes.string.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }
  render() {
    const { children, content, title } = this.props;
    const { isOpen } = this.state;
    return (
      <TetherComponent
        attachment="top center"
        constraints={[{
          to: 'scrollParent',
          attachment: 'together',
        }]}>
        {<DropdownWrapper isOpen={isOpen} onClick={() => { this.setState({ isOpen: !isOpen }); }}>
          {children}
        </DropdownWrapper>
        }
        { isOpen &&
          <DropDownContent isOpen={isOpen}>
            <DropdownHeader>{title}</DropdownHeader>
            {content}
          </DropDownContent>
        }
      </TetherComponent>
    );
  }
}

export default DropDown;
