import React, { PureComponent } from 'react';
import ErrorBoundary from '../../atoms/ErrorBoundary/index';
import IconArrowBack from '../Icons/Svg/IconArrowBack';

class BackIconButton extends PureComponent {
  render() {
    return (
      <ErrorBoundary>
        <IconArrowBack {...this.props} />
      </ErrorBoundary>
    );
  }
}

export default BackIconButton;
