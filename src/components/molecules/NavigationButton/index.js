import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Text from '../../atoms/Text/Text';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  position: relative;
  height: ${({ theme }) => theme.header.height || 0};
  padding-top: .3rem;
  cursor: pointer;
`;

const ActionIconWrapper = styled.div`
  position: absolute;
  ${({ position }) => (position === 'top' ? 'right: 17px; top: 5px;' : 'right: 17px; bottom: 28px;')}
`;

const IconText = styled(Text)`
  font-weight: 500;
  font-size: 12px;
`;

const NavigationButton = props => {
  const { icon, iconAction, actionPosition, text } = props;
  return (
    <Container>
      <ActionIconWrapper position={actionPosition}>
        {iconAction}
      </ActionIconWrapper>
      {icon}
      <IconText>{text}</IconText>
    </Container>
  );
};

NavigationButton.propTypes = {
  icon: PropTypes.node.isRequired,
  iconAction: PropTypes.node,
  actionPosition: PropTypes.string,
  text: PropTypes.string,
};

export default NavigationButton;
