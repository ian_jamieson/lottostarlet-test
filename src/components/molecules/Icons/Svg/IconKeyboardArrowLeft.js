// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconKeyboardArrowLeft = (props:Object) => (
  <Icon
    title="Left"
    icon="M658 686l-60 60-256-256 256-256 60 60-196 196z"
    {...props} />
);

IconKeyboardArrowLeft.propTypes = {
  ...Icon.propTypes,
};

export default IconKeyboardArrowLeft;
