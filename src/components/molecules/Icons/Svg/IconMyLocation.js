// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconMyLocation = (props:Object) => (
  <Icon
    title="Your Location"
    icon="M896 128l-322 768h-42l-112-292-292-112v-42z"
    {...props} />
);

IconMyLocation.propTypes = {
  ...Icon.propTypes,
};

export default IconMyLocation;
