// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconNotifications = (props:Object) => (
  <Icon
    title="Notifications"
    icon="M490.667 938.667c47.147 0 85.333-38.187 85.333-85.333h-170.667c0 47.147 38.187 85.333 85.333 85.333zM768 682.667v-234.667c0-131.2-91.093-240.64-213.333-269.653v-29.013c0-35.413-28.587-64-64-64s-64 28.587-64 64v29.013c-122.24 29.013-213.333 138.453-213.333 269.653v234.667l-85.333 85.333v42.667h725.333v-42.667l-85.333-85.333z"
    {...props} />
);

export default IconNotifications;

