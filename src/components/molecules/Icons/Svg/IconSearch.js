// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconSearch = (props:Object) => (
  <Icon
    title="Search"
    icon="M406 598c106 0 192-86 192-192s-86-192-192-192-192 86-192 192 86 192 192 192zM662 598l212 212-64 64-212-212v-34l-12-12c-48 42-112 66-180 66-154 0-278-122-278-276s124-278 278-278 276 124 276 278c0 68-24 132-66 180l12 12h34z"
    {...props} />
);

IconSearch.propTypes = {
  ...Icon.propTypes,
};

export default IconSearch;
