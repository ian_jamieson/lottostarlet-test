// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconLocationOn = (props:Object) => (
  <Icon
    title="Location"
    icon="M512 85.333c-164.907 0-298.667 133.76-298.667 298.667 0 224 298.667 554.667 298.667 554.667s298.667-330.667 298.667-554.667c0-164.907-133.76-298.667-298.667-298.667zM512 490.667c-58.88 0-106.667-47.787-106.667-106.667s47.787-106.667 106.667-106.667 106.667 47.787 106.667 106.667-47.787 106.667-106.667 106.667z"
    {...props} />
);

IconLocationOn.propTypes = {
  ...Icon.propTypes,
};

export default IconLocationOn;

