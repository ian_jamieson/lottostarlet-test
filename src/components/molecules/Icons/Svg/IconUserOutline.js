// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconUserOutline = (props:Object) => (
  <Icon
    title="User"
    icon="M512 554c114 0 342 58 342 172v128h-684v-128c0-114 228-172 342-172zM512 170c94 0 170 78 170 172s-76 170-170 170-170-76-170-170 76-172 170-172zM512 636c-126 0-260 62-260 90v46h520v-46c0-28-134-90-260-90zM512 252c-50 0-90 40-90 90s40 88 90 88 90-38 90-88-40-90-90-90z"
    {...props} />
);

IconUserOutline.propTypes = {
  ...Icon.propTypes,
};

export default IconUserOutline;
