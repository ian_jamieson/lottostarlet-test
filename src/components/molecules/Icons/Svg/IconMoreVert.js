// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconMoreVert = (props:Object) => (
  <Icon
    title="More"
    icon="M512 682c46 0 86 40 86 86s-40 86-86 86-86-40-86-86 40-86 86-86zM512 426c46 0 86 40 86 86s-40 86-86 86-86-40-86-86 40-86 86-86zM512 342c-46 0-86-40-86-86s40-86 86-86 86 40 86 86-40 86-86 86z"
    {...props} />
);

IconMoreVert.propTypes = {
  ...Icon.propTypes,
};

export default IconMoreVert;
