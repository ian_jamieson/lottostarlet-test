// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconAdd = (props:Object) => (
  <Icon
    title="Add"
    icon="M810 554h-256v256h-84v-256h-256v-84h256v-256h84v256h256v84z"
    {...props} />
);

IconAdd.propTypes = {
  ...Icon.propTypes,
};

export default IconAdd;

