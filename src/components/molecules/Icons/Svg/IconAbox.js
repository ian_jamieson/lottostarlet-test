// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconAbox = (props:Object) => (
  <Icon
    title="A-Box"
    icon="M583.851 560.853c-13.995 20.224-37.333 33.365-63.787 33.365-28.459 0-53.248-15.36-66.688-38.187l-154.624-258.859 213.248 129.493 469.333-384-397.483 518.187zM810.667 370.261v269.739h-170.667c0 70.827-57.6 128-128 128s-128-57.173-128-128h-170.667v-426.667h458.325l104.32-85.333h-563.072c-47.36 0-84.48 37.973-84.48 85.333l-0.341 471.936-0.085 125.397c0 46.933 37.547 85.333 84.907 85.333h597.76c46.933 0 85.333-38.4 85.333-85.333v-551.637l-85.333 111.232z"
    {...props} />
);

IconAbox.propTypes = {
  ...Icon.propTypes,
};

export default IconAbox;
