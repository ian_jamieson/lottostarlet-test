// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconCart = (props:Object) => (
  <Icon
    title="Cart"
    icon="M726 768c46 0 84 40 84 86s-38 84-84 84-86-38-86-84 40-86 86-86zM42 86h140l40 84h632c24 0 42 20 42 44 0 8-2 14-6 20l-152 276c-14 26-42 44-74 44h-318l-38 70-2 6c0 6 4 10 10 10h494v86h-512c-46 0-84-40-84-86 0-14 4-28 10-40l58-106-154-324h-86v-84zM298 768c46 0 86 40 86 86s-40 84-86 84-84-38-84-84 38-86 84-86z"
    {...props} />
);

IconCart.propTypes = {
  ...Icon.propTypes,
};

export default IconCart;
