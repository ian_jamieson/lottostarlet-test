// @flow
import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconChartBar = (props:Object) => (
  <Icon
    title="Notifications"
    icon="M810.667 128h-597.333c-47.147 0-85.333 38.187-85.333 85.333v597.333c0 47.147 38.187 85.333 85.333 85.333h597.333c47.147 0 85.333-38.187 85.333-85.333v-597.333c0-47.147-38.187-85.333-85.333-85.333zM384 725.333h-85.333v-298.667h85.333v298.667zM554.667 725.333h-85.333v-426.667h85.333v426.667zM725.333 725.333h-85.333v-170.667h85.333v170.667z"
    {...props} />
);

export default IconChartBar;

