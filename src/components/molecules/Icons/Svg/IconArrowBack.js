import React from 'react';
import Icon from '../../../atoms/IconSvg/index';

const IconArrowBack = props => (
  <Icon
    title="Back"
    icon="M854 470v84h-520l238 240-60 60-342-342 342-342 60 60-238 240h520z"
    {...props} />
);

IconArrowBack.propTypes = {
  ...Icon.propTypes,
};

export default IconArrowBack;
