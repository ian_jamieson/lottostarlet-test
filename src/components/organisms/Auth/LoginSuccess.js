import React from 'react';
import styled from 'styled-components';
import { Loader } from '../../atoms/Loader/Loader';

const Container = styled.div`
  text-align: center;
  padding: 1rem;
  position: relative;
  & h2 {
    font-size: 1.1rem;
    font-weight: 600;
  }
`;

const LoaderContainer = styled.div`
  padding: 3rem;
`;

const LoginSuccess = () => (
  <Container>
    <h2>You will be logged in</h2>
    <LoaderContainer>
      <Loader />
    </LoaderContainer>
  </Container>
);

export default LoginSuccess;
