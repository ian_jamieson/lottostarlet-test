import React, { PureComponent } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LoginForm from './LoginForm';
import LoginSuccess from './LoginSuccess';

class Auth extends PureComponent {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={LoginForm} />
          <Route path="/success" component={LoginSuccess} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Auth;
