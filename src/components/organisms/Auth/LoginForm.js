
import React from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { withFormik } from 'formik';
// import fetchProcesses from 'src/graphql/queries/processes/fetchProcesses.gql';
import Form from '../../atoms/Form/Form';
import Field from '../../molecules/Field/Field';
import Button from '../../atoms/Button/Button';
import Text from '../../atoms/Text/Text';
import Link from '../../atoms/Link/Link';

const ErrorMessage = styled.div`
  color: red;
  padding: 1rem;
  text-align: center;
  background: #FAD0D0;
`;

const FormContainer = styled.div`
  padding: 1rem;
`;

// import Checkbox from '../../atoms/Checkbox/Checkbox';
// import ErrorBoundary from '../../atoms/ErrorBoundary/index';

const FormLayout = ({ values, errors, touched, dirty, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
  <Form onSubmit={handleSubmit}>
    {(errors.email || errors.password) && <ErrorMessage>Please correct the marked fields</ErrorMessage>}
    <FormContainer>
      <Field
        label="Email address"
        type="text"
        name="email"
        placeholder="E-mail address"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.email}
        touched={touched.email}
        dirty={dirty.email}
        errors={errors.email}
        required />
      <Field
        label="Password"
        type="password"
        name="password"
        placeholder="Password"
        message={<Text>Forgot your password? <Link to="/">Request a new one.</Link></Text>}
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.password}
        touched={touched.password}
        dirty={dirty.password}
        errors={errors.password}
        required />
      <Button type="submit" color="primary" label="Login" />
      <Text>Not registered yet? <Link to="/">Register now for free.</Link></Text>
    </FormContainer>
  </Form>);

const LoginForm = withFormik({
  // Transform outer props into form values
  mapPropsToValues: props => ({ email: '', password: '' }),
  // Add a custom validation function (this can be async too!)
  validate: (values, props) => {
    const errors = {};
    if (!values.email) {
      errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address';
    }
    if (!values.password) {
      errors.password = 'Required';
    }
    return errors;
  },
  // Submission handler
  handleSubmit: (values, { props, setSubmitting, setErrors }) => {
    props.history.push('/success');
  },
})(FormLayout);

export default withRouter(LoginForm);
