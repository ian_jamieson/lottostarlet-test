import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { graphql, compose } from 'react-apollo';
import Moment from 'react-moment';
import { MultiSelect } from 'primereact/components/multiselect/MultiSelect';
import fetchProcesses from 'src/graphql/queries/processes/fetchProcesses.gql';
import deleteProcess from 'src/graphql/mutations/processes/deleteProcess.gql';

import { DataTable } from 'primereact/components/datatable/DataTable';
import { Column } from 'primereact/components/column/Column';
import ErrorBoundary from '../../atoms/ErrorBoundary/index';
import { Loader } from '../../atoms/Loader/Loader';
import Button from '../../atoms/Button/Button';


class ProcessesDataTableContainer extends Component {
  static propTypes = {
    deleteProcess: PropTypes.func.isRequired,
    afterChange: PropTypes.func,
  };
  static defaultProps = {
    afterChange: null,
  };
  /**
   * Set our default state
   */
  constructor(props) {
    super(props);
    this.state = { filters: {} };
    this.onColorChange = this.onColorChange.bind(this);
    this.onFilter = this.onFilter.bind(this);
  }

  onFilter(e) {
    this.setState({ filters: e.filters });
  }

  onColorChange(e) {
    const filters = this.state.filters;
    filters.color = { value: e.value };
    this.setState({ filters });
  }
  /**
   * Render our list
   */

  linkTemplate(rowData) {
    return <Link to={`/abox/${rowData.id}`}>{rowData.title}</Link>;
  }
  createdTemplate(rowData) {
    return <Moment fromNow>{rowData.createdAt}</Moment>;
  }
  updatedTemplate(rowData) {
    return <Moment fromNow>{rowData.updatedAt}</Moment>;
  }
  deleteTemplate(rowData, column) {
    return <Button onClick={() => column.action(rowData.id)}>delete</Button>;
  }
  deleteRow = id => {
    // console.log('deleteRow', id);
    this.props.deleteProcess({ variables: { ID: id } }).then(this.props.afterChange);
  };


  render() {
    const { data: { allPosts, loading } } = this.props;
    // console.log('props', this.props);

    const colors = [
      { label: 'White', value: 'White' },
      { label: 'Green', value: 'Green' },
      { label: 'Silver', value: 'Silver' },
      { label: 'Black', value: 'Black' },
      { label: 'Red', value: 'Red' },
      { label: 'Maroon', value: 'Maroon' },
      { label: 'Brown', value: 'Brown' },
      { label: 'Orange', value: 'Orange' },
      { label: 'Blue', value: 'Blue' },
    ];

    const colorFilter = <MultiSelect style={{ width: '100%' }} className="ui-column-filter" value={this.state.filters.color ? this.state.filters.color.value : null} options={colors} onChange={this.onColorChange} />;

    if (loading) { return <Loader />; }

    return (
      <ErrorBoundary>
        <DataTable
          value={allPosts || []}
          responsive
          scrollable
          scrollHeight="100%"
          // paginator
          // rows={20}
          sortMode="multiple"
          filters={this.state.filters}
          onFilter={this.onFilter}>
          <Column columnKey="title" field="title" header="Title" sortable filter body={this.linkTemplate} />
          <Column columnKey="description" field="description" header="Description" sortable filter />
          <Column columnKey="createdAt" field="createdAt" header="Created" sortable filter body={this.createdTemplate} />
          <Column columnKey="updatedAt" field="updatedAt" header="Updated" sortable filter body={this.updatedTemplate} />
          <Column header="Color" sortable filter filterElement={colorFilter} filterMatchMode="in" />
          <Column header="Action" action={this.deleteRow} body={this.deleteTemplate} />
        </DataTable>
      </ErrorBoundary>
    );
  }
}

const ProcessesDataTable = compose(
  graphql(fetchProcesses),
  graphql(deleteProcess, {
    name: 'deleteProcess',
    options: { refetchQueries: ['fetchProcesses'] } }),
)(ProcessesDataTableContainer);


export default ProcessesDataTable;
