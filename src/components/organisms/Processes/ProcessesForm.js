
import React from 'react';
import { withFormik } from 'formik';
import { graphql } from 'react-apollo';

import createProcess from 'src/graphql/mutations/processes/createProcess.gql';
// import fetchProcesses from 'src/graphql/queries/processes/fetchProcesses.gql';
import Form from '../../atoms/Form/Form';
import Field from '../../molecules/Field/Field';
import Button from '../../atoms/Button/Button';
// import Checkbox from '../../atoms/Checkbox/Checkbox';
// import ErrorBoundary from '../../atoms/ErrorBoundary/index';


const FormLayout = ({ values, errors, touched, dirty, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
  <Form onSubmit={handleSubmit}>
    <Field
      label="Title"
      type="text"
      name="title"
      onChange={handleChange}
      onBlur={handleBlur}
      value={values.title}
      touched={touched.title}
      dirty={dirty.title}
      errors={errors.title}
      required />
    <Field
      label="Description"
      type="text"
      name="description"
      onChange={handleChange}
      onBlur={handleBlur}
      value={values.description}
      touched={touched.description}
      dirty={dirty.description}
      errors={errors.description}
      required />
    <Button type="submit" color="primary">Submit</Button>
  </Form>);

const ProcessForm = withFormik({
  // Transform outer props into form values
  mapPropsToValues: props => ({ title: '', description: '' }),
  // Add a custom validation function (this can be async too!)
  validate: (values, props) => {
    const errors = {};
    if (!values.title) {
      errors.title = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.title)) {
      errors.title = 'Invalid email address';
    }
    return errors;
  },
  // Submission handler
  handleSubmit: (values, { props, setSubmitting, setErrors /* setValues, setStatus, and other goodies */ }) => {
    props.mutate({
      variables: {
        title: values.title,
        description: values.description,
      },
    });
  },
})(FormLayout);

export default graphql(createProcess, { options: { refetchQueries: ['fetchProcesses'] } })(ProcessForm);
