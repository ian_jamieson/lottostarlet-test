import React from 'react';
import styled from 'styled-components';
import ErrorBoundary from '../../atoms/ErrorBoundary';
import Stats from '../../atoms/Stats/index';

const FooterStyle = styled.footer`
  grid-area: footer;
  height: 50px;
  background: #111;
  font-size: .8rem;
  display: flex;
  align-items: center;
  padding: 0 1rem;
  box-shadow: 0 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12);
  z-index: 10;
  & > * {
    align-self:stretch;
    display:flex;
    align-items:center;
  }
`;

const GlobalFooter = () => (<ErrorBoundary>
  <FooterStyle>
    <Stats />
  </FooterStyle>
</ErrorBoundary>);

export default GlobalFooter;
