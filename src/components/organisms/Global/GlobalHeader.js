import React, { PureComponent } from 'react';
import styled from 'styled-components';
import NavigationButton from '../../molecules/NavigationButton';
import IconUserOutline from '../../molecules/Icons/Svg/IconUserOutline';
import IconAdd from '../../molecules/Icons/Svg/IconAdd';
import IconCart from '../../molecules/Icons/Svg/IconCart';
import DropDown from '../../molecules/DropDown/index';
import Auth from '../Auth/index';
import CounterRounded from '../../atoms/CounterRounded/index';

const Header = styled.header`
  grid-area: header;
  display: block;
  height: ${({ theme }) => theme.header.height || 0};
  text-align: center;
  background: white;
  border-bottom: solid 1px rgba(209,209,209,1);
  z-index: 10;
`;

const Navigation = styled.div`
  width: auto;
  margin: 0 auto;
`;

const NavItem = styled.div`
  display: inline-block;
  padding: 0 1rem;
  height: ${({ theme }) => theme.header.height || 0};
`;

class GlobalHeader extends PureComponent {
  render() {
    return (
      <Header className="Header">
        <Navigation>
          <NavItem>
            <NavigationButton
              text="Create Account"
              icon={<IconUserOutline size="32" />}
              iconAction={<IconAdd size="14" backgroundColor="#A68A41" color="white" rounded />} />
          </NavItem>
          <NavItem>
            <DropDown title="Secure Login" content={<Auth />}>
              <NavigationButton
                text="Login"
                icon={<IconUserOutline size="32" />} />
            </DropDown>
          </NavItem>
          <NavItem>
            <NavigationButton
              text="ShoppingCart"
              icon={<IconCart size="32" />}
              iconAction={<CounterRounded number={2} />}
              actionPosition="top" />
          </NavItem>
        </Navigation>
      </Header>
    );
  }
}

export default GlobalHeader;
