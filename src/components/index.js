// @flow
/* eslint-disable no-unused-expressions */
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ThemeProvider, injectGlobal } from 'styled-components';
import theme from '../themes/dark';

import GlobalTemplate from './templates/GlobalTemplate';
import HomePage from './pages/HomePage/index';


injectGlobal`
  *, *:after, *:before {
    box-sizing: border-box;
  }
  @import url('https://fonts.googleapis.com/css?family=Poppins');
  html {
    height: 100vh;
  }
  body, #main {
    min-height: 100vh;
    margin: 0;
    padding: 0;
    border: 0;
    font-family: 'Poppins', sans-serif;
    font-size: 12px;
    overflow: hidden;
  }
  @media screen and (min-width: 2000px) {
    body {
      font-size: 16px;
    }
  }
`;

// ----------------------

export default () => (
  <ThemeProvider theme={theme}>
    <GlobalTemplate>
      <Switch>
        <Route exact path="/" component={HomePage} />
      </Switch>
    </GlobalTemplate>
  </ThemeProvider>
);
