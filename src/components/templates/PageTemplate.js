// flow
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { ChildrenProp } from '../../utils/props';
import GlobalHeader from '../organisms/Global/GlobalHeader';
import GlobalFooter from '../organisms/Global/GlobalFooter';

const Grid = styled.main`
  height: 100vh;
  background: rgba(255,255,255,1);
  display: grid;
  grid-template-rows: auto 1fr auto;
  grid-template-areas: "header"
                       "content"
                       "footer"
  ;
  overflow: hidden;
`;

const Content = styled.article`
  grid-area: content;
  overflow: auto;
`;


const PageTemplate = props => {
  const { children, header, footer, title } = props;
  return (
    <Grid>
      { header && header ? header : <GlobalHeader title={title} />}
      <Content>{ children }</Content>
      { footer && footer ? footer : <GlobalFooter />}
    </Grid>
  );
};

PageTemplate.propTypes = {
  children: ChildrenProp.isRequired,
  header: ChildrenProp,
  title: PropTypes.string,
};

export default PageTemplate;
