import React, { PureComponent } from 'react';
import styled from 'styled-components';

import { ChildrenProp } from '../../utils/props';


const Wrapper = styled.div`
  height: 100vh;
  color: ${({ theme }) => theme.palette.text};
  a {
    color: ${({ theme }) => theme.palette.link};
    text-decoration: none;
  }
`;

class GlobalTemplate extends PureComponent {
  static propTypes = {
    children: ChildrenProp.isRequired,
  };
  render() {
    return (
      <Wrapper>
        {this.props.children}
      </Wrapper>
    );
  }
}

export default GlobalTemplate;

