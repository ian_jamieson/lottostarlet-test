// @flow
import PropTypes from 'prop-types';

const LabelProps = {
  children: PropTypes.any,
};

const LabelDefaultProps = {

};

export { LabelProps, LabelDefaultProps };
