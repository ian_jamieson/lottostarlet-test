import React from 'react';
import styled from 'styled-components';

const ContentStyle = styled.div`
    display: block;
    ${({ padded }) => (padded ? 'padding: 1rem' : '')};
    ${({ fullHeight }) => (fullHeight ? 'height: 100%' : '')};
`;

const ContentArea = props => {
  const { padded, fullHeight, children, ...rest } = props;
  return (
    <ContentStyle
      padded={padded}
      fullHeight={fullHeight}
      {...rest}>
      {children}
    </ContentStyle>
  );
};

export default ContentArea;
