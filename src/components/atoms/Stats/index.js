import React from 'react';
import styled, { css } from 'styled-components';

const reset = css`
  margin:0; padding:0;
  font-weight: 600;
  list-style: none;
  font-size: inherit;
`;

const Environment = styled.ul`
  ${reset};
`;
const EnvKey = styled.li`
  ${reset};
  span {
    font-weight: normal;
  }
`;

const Stats = () => {
  const info = [
    ['Environment', process.env.NODE_ENV],
  ];

  return (
    <Environment>
      {info.map(([key, val]) => (
        <EnvKey key={key}>{key}: <span>{val}</span></EnvKey>
      ))}
    </Environment>
  );
};

export default Stats;
