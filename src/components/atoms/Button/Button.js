import React from 'react';
import { lighten } from 'polished';
import styled from 'styled-components';

const ButtonStyle = styled.button`
  border:0; outline: 0;
  padding: 0 .8rem;
  margin: 0 0.1rem;
  font-size: 1rem;
  font-weight: 600;
  border-radius: .3rem;
  display: block;
  width: 100%;
  height: 40px;
  line-height: 38px;
  color: white;
  cursor: pointer;
  background: ${({ theme, color, disabled }) => (color && !disabled ? theme.palette[color] : 'transparent')};
  box-shadow: inset 0 -3px 0 rgba(0,0,0,0.1);
  &:hover {
    background: ${({ theme, color, disabled }) => (color && !disabled ? lighten(0.1, theme.palette[color]) : 'transparent')};
  }
`;

const Button = ({ color, label, ...props }) => (<ButtonStyle {...props} color={color}>{label}</ButtonStyle>);

export default Button;
