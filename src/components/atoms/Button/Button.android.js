import React, { Component } from 'react';
import { Button as NativeButton } from 'react-native';

class Button extends Component {
  render() {
    const { children, ...rest } = this.props;
    return <NativeButton {...rest}>{children}</NativeButton>;
  }
}

export default Button;
