
import React, { PureComponent } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const IconStyle = styled.div`
  display: inline-block;
  vertical-align: middle;
  line-height: ${({ size }) => (size || 1)}px;
  text-align: center;
  cursor: pointer;
  padding: 0;
  outline: 0;
  border: none;
  margin: ${({ noMargin }) => (noMargin ? 0 : '.5em')};
  & > svg {
    display: inline-block;
    vertical-align: middle;
    margin: 0 auto;
  }
  {/* Over-rides for props */}
   ${({ rounded, size }) => (rounded ? `
    width: ${size * 1.25}px;
    height: ${size * 1.25}px;
  ` : `
    width: ${size}px;
    height: ${size}px;
  `)};
  ${({ rounded, backgroundColor }) => (rounded ? `
    background: ${backgroundColor || 'black'};
    border: solid 1px rgba(0,0,0,0.2);
    border-radius: 50%;
  ` : '')};
`;

class Icon extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    size: PropTypes.string,
    icon: PropTypes.string,
    color: PropTypes.string,
    backgroundColor: PropTypes.string,
    className: PropTypes.string,
    rounded: PropTypes.bool,
    noMargin: PropTypes.bool,
  };
  static defaultProps = {
    title: 'Icon',
    icon: '',
    size: '32',
    color: '',
    backgroundColor: '',
    className: '',
    rounded: false,
    noMargin: false,
  };
  render() {
    const { icon, size, backgroundColor, color, title, rounded, noMargin, className, ...rest } = this.props;

    // Adjust the icon size if rounded
    const iconSize = rounded ? Math.round((size / 1.2)) : size;

    return (
      <IconStyle backgroundColor={backgroundColor} rounded={rounded} size={size} noMargin={noMargin} className={`IconSvg ${className}`} {...rest}>
        <svg width={iconSize} height={iconSize} viewBox="0 0 1024 1024" preserveAspectRatio="xMaxYMax meet" aria-labelledby="title">
          <title id="title">{title}</title>
          <path fill={color} d={icon} />
        </svg>
      </IconStyle>
    );
  }
}

export default Icon;
