import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const CounterStyle = styled.span`
  display: inline-block;
  font-size: .8em;
  line-height: 17px;
  border-radius: 50%;
  width: 17.5px;
  height: 17.5px;
  background: #A68A41;
  color: white;
  text-shadow: 1px -1px rgba(255,255,255,0.1);
`;

const CounterRounded = ({ number }) => <CounterStyle>{number}</CounterStyle>;

CounterRounded.propTypes = {
  number: PropTypes.number.isRequired,
};

export default CounterRounded;
