import React from 'react';
import { Checkbox as UiCheckbox } from 'primereact/components/checkbox/Checkbox';
import styled from 'styled-components';


const CheckboxStyle = styled(UiCheckbox)`
 
`;

const Checkbox = ({ ...props }) => (<CheckboxStyle {...props} />);

export default Checkbox;
