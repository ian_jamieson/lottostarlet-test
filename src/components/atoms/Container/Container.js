// import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: block;
  padding: 1rem;
`;

export default Container;
