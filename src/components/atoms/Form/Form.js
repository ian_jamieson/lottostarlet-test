// @flow
import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { ChildrenProp } from '../../../utils/props';

const FormStyle = styled.form`
  display: block;
`;

class Form extends PureComponent<Object, Object> {
  static propTypes = {
    children: ChildrenProp,
  };
  render() {
    const { children, ...rest } = this.props;
    return (
      <FormStyle {...rest}>{children}</FormStyle>
    );
  }
}

export default Form;
