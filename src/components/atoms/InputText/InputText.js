import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const InputStyle = styled.input`
  width: 100%;
  padding: 0 .7rem;
  outline: none;
  border-radius: .3rem;
  height: 40px;
  color: #C8C8C8;
  ${({ errors }) => (errors ? 'background:white; border: solid 1px red; box-shadow: 0px 0px 4px red; color: red;' : 'border: solid 1px #ccc;')};
  &::placeholder {
    color: #B4B4B4;
  }
`;

class InputText extends PureComponent {
  static propTypes = {
    name: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    errors: PropTypes.string,
  };
  static defaultProps = {
    name: '',
    value: '',
    placeholder: '',
    onChange: null,
    errors: null,
  };
  onChange = event => {
    this.value = event.target.value;
    if (this.props.onChange) { this.props.onChange(event); } // Pass the onChange to the parent
    // console.log('inputValue', this.value);
  };
  render() {
    const { name, placeholder, value, errors, ...rest } = this.props;

    return (
      <InputStyle
        name={name}
        placeholder={placeholder}
        errors={errors}
        value={value || this.value || ''}
        onChange={this.onChange}
        {...rest} />
    );
  }
}

export default InputText;
