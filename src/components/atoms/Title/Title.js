import React from 'react';
import styled from 'styled-components';

const TitleBase = styled.span`
  font-weight: 600;
  margin: 0;
`;

const Title = props => {
  const { as, children } = props;
  const setTitleAs = props.as;
  const TitleStyle = TitleBase.withComponent(setTitleAs);

  return (
    <TitleStyle as={as} {...props}>
      {children}
    </TitleStyle>
  );
};

export default Title;
