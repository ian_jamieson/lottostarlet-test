// @flow
import styled from 'styled-components';

const Text = styled.span`
  font-size: inherit;
`;

export default Text;
