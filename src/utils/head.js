import React from 'react';

export default (props: Object) => (
  <div>
    <title>{props.title}</title>
    {props.children}
    <meta charSet="utf-8" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  </div>
);
