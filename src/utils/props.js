import PropTypes from 'prop-types';

export const ChildrenProp = PropTypes.oneOfType([
  PropTypes.any,
]);

export const SizeProps = PropTypes.oneOf(
  [
    'xs',
    'sm',
    'md',
    'lg',
    'xl',
  ],
);
