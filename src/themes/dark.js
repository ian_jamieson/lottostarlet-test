import sizes from './core/sizes';
import colors from './core/colors';

const theme = {};
theme.sizes = sizes;
theme.color = colors;

theme.palette = {
  base: '#333',
  text: '#888',
  link: '#0071BD',
  primary: '#B08C30',
};

theme.header = {
  height: '78px',
};

export default theme;
