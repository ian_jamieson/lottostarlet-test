const colors = {
  primary: '#00a99d',
  secondary: '#066ab1',
  info: '#4FC3F7',
  success: '#81C784',
  warning: '#FF8A65',
  alert: '#FFB74D',
  danger: '#e57373',
  error: '#e57373',
  white: '#FFFFFF',
};

export default colors;
