import sizes from './core/sizes';
import colors from './core/colors';

const theme = {};
theme.sizes = sizes;
theme.color = colors;

export default theme;
